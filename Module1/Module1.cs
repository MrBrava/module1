﻿using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 module1 = new Module1();
        }


        public int[] SwapItems(int a, int b)
        {
            int[] array = new int[2];
            array[0] = b;
            array[1] = a;
            return array;
        }

        public int GetMinimumValue(int[] input)
        {
            int min = input[0];
            for (int i = 1; i < input.Length; i++){
                if (input[i] < min)
                    min = input[i];
            }
            return min;
        }
    }
}
